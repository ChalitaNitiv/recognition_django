# detected verify idcard picture => required picture selfie wioth id card
from multiprocessing import Process
import time
from PIL import Image
import face_recognition
import signal

def recognition_idcard(img_name):
    filename = img_name
    print(777,filename)

    image = face_recognition.load_image_file(filename)
    #locate face from image
    face_locations = face_recognition.face_locations(image)
    face_locations_deep = face_recognition.face_locations(image, number_of_times_to_upsample=0, model="cnn")
    print("I found {} face(s) in this photograph.".format(len(face_locations)))

    img_arr = []
    for face_location in face_locations:

        # Print the location of each face in this image
        top, right, bottom, left = face_location
        print("A face is located at pixel location Top: {}, Left: {}, Bottom: {}, Right: {}".format(top, left, bottom, right))

        # You can access the actual face itself like this:
        face_image = image[top:bottom, left:right]
        pil_image = Image.fromarray(face_image)
        # pil_image.show()
        img_arr.append(face_image)

    try:
        first_face_encoding = face_recognition.face_encodings(img_arr[0])[0]
        second_face_encoding = face_recognition.face_encodings(img_arr[1])[0]
    except IndexError:
        print("Cannot detected Id card picture.")
        return False
        # quit()

    # results is an array of True/False telling if the unknown face matched anyone in the known_faces array
    results = face_recognition.compare_faces([first_face_encoding], second_face_encoding)
    print(results[0])
    return results[0]
