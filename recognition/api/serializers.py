from rest_framework import serializers
from django import forms
from .models import UserProfile, ImageUser

class UserSerializer(serializers.Serializer):
    # class Meta:
    #     model = ImageUser
    #     fields = 'user_id', 'image_uri', 'status',
    lasernumber = serializers.CharField(max_length=12)
    idnumber = serializers.CharField(max_length=13)
    frontcard_uri = serializers.CharField()
    backcard_uri = serializers.CharField()

    # print(12,UserProfile.objects.all())

    def create(self, validated_data):
        return UserProfile.objects.create(**validated_data)

class ImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ImageUser
        fields = 'user_id', 'image_uri', 'status',
    # user_id = serializers.IntegerField()
    # image_uri = serializers.ImageField()
    # status = serializers.BooleanField()
    # print(25,data)
    def create(self, validated_data):
        return ImageUser.objects.create(**validated_data)
