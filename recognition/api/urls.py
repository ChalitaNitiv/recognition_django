from django.urls import path

from .views import UserView, ImageView


app_name = "users"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('users/', UserView.as_view()),
    path('images/', ImageView.as_view()),
]
