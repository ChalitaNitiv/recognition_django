# Generated by Django 2.2.4 on 2019-09-02 03:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_auto_20190902_1048'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='deleted_at',
        ),
    ]
