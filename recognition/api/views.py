import os
import sys
from PIL import Image
import time
import json

from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import UserProfile, ImageUser
from .serializers import UserSerializer, ImageSerializer
from .forms import PostForm
from django.core.files.storage import FileSystemStorage
from .face_ex import recognition_idcard


class UserView(APIView):
    def get(self, request):
        users = UserProfile.objects.all()
        # the many param informs the serializer that it will be serializing more than a single article.
        serializer = UserSerializer(users, many=True)
        return Response({"users": serializer.data})
    def post(self, request):
        # print(27,request.data)
        fs = FileSystemStorage()
        uploaded_file = request.FILES['image_front']
        uploaded_file2 = request.FILES['image_back']

        genTime = int(time.time())
        imgName = fs.save('images/' +  str(genTime) + "_" + uploaded_file.name , uploaded_file)
        imgName2 = fs.save('images/' +  str(genTime) + "_" + uploaded_file2.name , uploaded_file2)

        # filename = "./media/" + imgName
        # filename2 = "./media/" + imgName2
        # print(29,request.body.idnumber)
        user_idnumber = request.data.get('idnumber')
        user_lasernumber = request.data.get('lasernumber')
        print(17,user_idnumber,user_lasernumber)
        obj = {
            "idnumber": user_idnumber,
            "lasernumber": user_lasernumber,
            "frontcard_uri": imgName,
            "backcard_uri": imgName2,
        }
        # Create an user from the above data
        serializer = UserSerializer(data=obj)
        if serializer.is_valid(raise_exception=True):
            user_saved = serializer.save()
        return Response({"success": "User idcard number '{}' created successfully".format(user_saved.idnumber),
                         "user_id": user_saved.id})
        # return Response("Success")

# Create your views here.
class ImageView(APIView):
    def get(self, request):
        images = ImageUser.objects.all()
        # the many param informs the serializer that it will be serializing more than a single article.
        serializer = ImageSerializer(images, many=True)
        return Response({"images": serializer.data})
    def post(self, request):
        if request.method == 'POST':
            image = ImageUser()
            fs = FileSystemStorage()
            uploaded_file = request.FILES['image']
            # print(62,request.FILES['image_back'])
            # print(63,request.FILES['image'])
            genTime = int(time.time())
            imgName = fs.save('images/' + uploaded_file.name , uploaded_file)

            filename = "./media/" + imgName
            im = Image.open(filename)
            img_size = os.stat(filename).st_size
            print("Before Size => ",os.stat(filename).st_size)
            print("Before Resolution Size => ",im.size[0], im.size[1])
            # print(65, './media/images/' + "compress_" + str(genTime) + "_"  + uploaded_file.name)
            # return Response("Test")

            if img_size > 5242880:
                os.remove("media/images/" + uploaded_file.name)
                return Response("Image size is too big. Please upload size less than 5 mb.")
            # 65 quality is minimum average
            if img_size < 665600 and img_size > 102400:
                im.save('./media/images/' + "compress_" + str(genTime) + "_"  + uploaded_file.name  , optimize=True, quality=65)
            elif img_size < 5242880 and img_size > 2621440:
                resize_b = im.size[0] * (35/100), im.size[1] * (35/100)
                im.thumbnail(resize_b, Image.ANTIALIAS)
                im.save('./media/images/' + "resize_" + str(genTime) + "_"  + uploaded_file.name )
                print("After Resolution Size => ",im.size[0] * (35/100),im.size[1] * (35/100))

                im2 = Image.open('./media/images/' + "resize_" + str(genTime) + "_"  + uploaded_file.name)
                im2.save('./media/images/' + "compress_" + str(genTime) + "_"  + uploaded_file.name  , optimize=True, quality=20)
                os.remove('media/images/' + "resize_" + str(genTime) + "_"  + uploaded_file.name)
                # im.save('./media/images/' + "compress_" + str(genTime) + "_"  + uploaded_file.name  , optimize=True, quality=65)
            elif img_size < 2621440 and img_size > 665600:
                resize = im.size[0] * (40/100), im.size[1] * (40/100)
                im.thumbnail(resize, Image.ANTIALIAS)
                im.save('./media/images/' + "resize_" + str(genTime) + "_"  + uploaded_file.name )
                print("After Resolution Size => ",im.size[0] * (40/100),im.size[1] * (40/100))

                im2 = Image.open('./media/images/' + "resize_" + str(genTime) + "_"  + uploaded_file.name)
                im2.save('./media/images/' + "compress_" + str(genTime) + "_"  + uploaded_file.name  , optimize=True, quality=20)
                os.remove('media/images/' + "resize_" + str(genTime) + "_"  + uploaded_file.name)
            else: #image size < 100 kb
                im.save('./media/images/' + "compress_" + str(genTime) + "_"  + uploaded_file.name)

            os.remove("media/images/" + uploaded_file.name)
            full_name = './media/images/' + "compress_" + str(genTime) + "_"  + uploaded_file.name
            print("Afetr Size => ",os.stat(full_name).st_size)
            print(56,full_name)
            # result2 = timeout()
            result = recognition_idcard(full_name)
            # rusult = "2525252"
            print(59,result)
            # print(64,result2)
            image.image_uri = imgName
            image.status = result
            image.user_id = request.POST.get('user_id')
            image.save()
        return Response({"success": "Image created successfully and image status is '{}'".format(result),
                         "status": result,
                         "user_id": int(image.user_id)
                        })

# Create your views here.
