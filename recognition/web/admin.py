from django.contrib import admin
from .models import UserIdcard, ImageVerify

admin.site.register(UserIdcard)
admin.site.register(ImageVerify)


# Register your models here.
