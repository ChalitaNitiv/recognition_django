from django.db import models

class UserIdcard(models.Model):
    lasernumber = models.CharField(max_length=12)
    idnumber = models.CharField(max_length=13)
    frontcard_uri = models.ImageField(upload_to='images', default=None, null=True)
    backcard_uri = models.ImageField(upload_to='images', default=None, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.idnumber

class ImageVerify(models.Model):
    user = models.ForeignKey('UserIdcard', related_name='users', on_delete=models.CASCADE)
    image_uri = models.ImageField(upload_to='images')
    status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __all__(self):
        return self.image_uri


# Create your models here.
