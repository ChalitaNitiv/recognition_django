from django.urls import path
from .views import UserView, ImageView


app_name = "web"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('user/', UserView.as_view()),
    path('image/', ImageView.as_view()),
]
